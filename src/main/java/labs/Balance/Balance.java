package labs.Balance;

import org.springframework.web.bind.annotation.ResponseBody;

public class Balance {

    public Balance() {

        this.money = Double.parseDouble("1000");
    }

    public Balance(Double money) {
        this.money = money;
    }


    //количество денег
    private Double money;

    public double getPenny() {
        return money;
    }

    public void setPenny(double money) {
        this.money = money;
    }
}
