package labs.Balance;


import org.springframework.stereotype.Service;

@Service
public class BalanceService {
    Balance b;
    public BalanceService() {
        this.b = new Balance();
    }
    public double get() {
        return b.getPenny();
    }
    public void set(double money) {
        b.setPenny(money);
    }
}
