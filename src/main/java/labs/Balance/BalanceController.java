package labs.Balance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.json.*;

@Controller
public class BalanceController {

    BalanceService bs;

    @Autowired
    public BalanceController(BalanceService bs) {
        this.bs = bs;
    }

    @RequestMapping(value="balance", method = RequestMethod.GET)
    @ResponseBody
    public String getBalance() {
        return "Баланс: " + bs.get();
    }

    @RequestMapping(value="balance/{coin}", method = RequestMethod.PUT)
    @ResponseBody
    public String setBalance(@PathVariable String coin){
        bs.set(Double.parseDouble(coin));
        return "Баланс " + coin + " успешно установлен.";

    }
}