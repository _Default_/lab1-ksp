package labs.Cart;

import labs.Currency.CurrencyService;
import labs.Item.Item;
import labs.Item.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CartController {

    CartService cs;
    CurrencyService currencyService;

    @Autowired
    public CartController(CartService cs, CurrencyService currencyService) {
        this.cs = cs;
        this.currencyService = currencyService;
    }


    @RequestMapping(value="cart/pet/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putPet(@PathVariable String id) {
        String response = cs.putItem(Integer.parseInt(id), 0, Item.Types.Pet);
        return (response);
    }

    @RequestMapping(value="cart/staff/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public String putStaff(@PathVariable String id) {
        String response = cs.putItem(Integer.parseInt(id), 0, Item.Types.Staff);
        return (response);
    }

    @RequestMapping(value="cart/item/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItem(@PathVariable String id) {
        String response = cs.removeItem(Integer.parseInt(id));
        return response;
    }
    @RequestMapping(value="cart/itemid/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItemId(@PathVariable String id) {
        String response = cs.removeItemId(Integer.parseInt(id));
        return response;
    }



    @RequestMapping(value="cart/item", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItem() {
        cs.removeItem();
        return ("Корзина очищена.");
    }

    @RequestMapping(value="cart/petall", method = RequestMethod.DELETE)
    @ResponseBody
    public String removePets() {
        String response = cs.removeItem(Item.Types.Pet);
        return response;
    }

    @RequestMapping(value="cart/staffall", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeStaffs() {
        String response = cs.removeItem(Item.Types.Staff);
        return response;
    }

    @RequestMapping(value="cart", method = RequestMethod.GET)
    @ResponseBody

    public List<CartElement> get() {
        return cs.getCart();
    }
}
