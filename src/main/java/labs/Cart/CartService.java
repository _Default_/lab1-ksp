package labs.Cart;

import labs.Balance.Balance;
import labs.Item.Item;
import labs.Item.ItemService;
import labs.User;
import org.springframework.stereotype.Service;

import java.net.Proxy;
import java.util.*;

import static labs.Item.Item.Types.Pet;
import static labs.Item.Item.Types.Staff;


@Service
public class CartService {
    List<CartElement> cartL;
    Cart cart;
    User us;
    Balance balance;
    ItemService is = new ItemService();
    private int index = 1;

    public CartService() {


        balance = new Balance();
        this.cartL = new LinkedList<>();
        this.cartL.add(new CartElement(1, 3));
        this.cartL.add(new CartElement(2, 2));
        cart = new Cart(us, cartL);
        us = new User(1, User.Role.User, balance, cart);

    }

    public List<CartElement> getCart()
    {
        return cartL;
    }

    public CartElement getCartElement(int id) {
        for(CartElement it : getCart())
            if(it.getId() == id)
                return it;
        return null;
    }

    public void addElement(CartElement input) {
        cartL.add(input);
    }

    public String putItem(int id, int personId, Item.Types types) {
        int i;
        ArrayList<Integer> arrayList= new ArrayList<>();
        int max = 0;
        for (CartElement it: getCart())
        {
            if (it.getId()>max)
            {
             max = it.getId();
            }
        }
        for (i = 0; i < max; i++)
            arrayList.add(i, 0);
        for (CartElement it: getCart())
        {
            arrayList.set(it.getId()-1, 1);
        }
        for (i = 0; i < arrayList.size(); i++){
            if (arrayList.get(i)!= 1) {
                index = i + 1;
                break;
            }
            index = max + 1;
        }

        if (types == Pet){
        if (is.getItems(id, types)!= null) {
            addElement(new CartElement(index, id));
            return ("Питомец с id " + id + " успешно добавлен.");
        }
        else return ("Данный Id не принадлежит питомцу.");}
        if (types == Staff){
            if (is.getItems(id, types)!= null) {
                addElement(new CartElement(index++, id));
                return ("Товар с id " + id + " успешно добавлен.");
            }
            else return ("Данный Id не принадлежит товару.");}
        return "Error 404. Page not found.";
    }

    public String removeItem(int id) {
        Iterator<CartElement> iter = cartL.iterator();
        while (iter.hasNext()) {
            if(iter.next().getId() == id){
                iter.remove();
                return "Товар с Id " + id + " успешно удален из корзины.";
            }
        }
        return "Данного предмета нет в корзине.";
    }

    public String removeItemId(int id) {
        Iterator<CartElement> iter = cartL.iterator();
        while (iter.hasNext()) {
            if(iter.next().getItem_id() == id){
                iter.remove();
                return "Товар с Id " + id + " успешно удален из корзины.";
            }
        }
        return "Данного предмета нет в корзине.";
    }

    public String removeItem(Item.Types types) {
        String resp = new String();
        List<Integer> del = new LinkedList<>();
        Iterator<CartElement> iter = cartL.iterator();
        while (iter.hasNext()) {
            CartElement tmp = iter.next();
            if(is.getItems(tmp.getItem_id()).getType() == types){
                del.add(tmp.getItem_id());
            }
        }
        while (!del.isEmpty())
        {
            resp = resp + removeItemId((((LinkedList<Integer>) del).removeFirst())) + "\n";
        }
        if (resp.isEmpty())
        return "Данного предмета нет в корзине.";
        else return resp;
    }

    public void removeItem() {
        cartL.clear();
    }
}