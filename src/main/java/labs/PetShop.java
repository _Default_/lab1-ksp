package labs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class PetShop {
    public static void main(String[] Args){
        SpringApplication.run(PetShop.class); }

}
