package labs.Item;

public class Item {
    private int id;
    private String name;
    private Types type;
    private int age;
    private int cost;

    public Item(int id, String name, Types type, int age, int cost) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.age = age;
        this.cost = cost;
   }



    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Types getType() {
        return type;
    }

    public int getAge() {
        return age;
    }

    public int getCost() {
        return cost;
    }

    public enum Types {
        notSupprt,
        Pet,
        Staff
    }
}