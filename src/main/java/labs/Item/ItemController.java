package labs.Item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ItemController {

    private final ItemService p;

    @Autowired
    public ItemController(ItemService p) {
        this.p = p;
    }

    @RequestMapping(value="items/pets", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getPets() {
        return p.getPets();
    }

    @RequestMapping(value="items/staffs", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getStuffs(){
        return p.getStuffs();
    }

    @RequestMapping(value="items/pets/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Item getPets(@PathVariable String id) {
        return p.getPets(Integer.parseInt(id));
    }

    @RequestMapping(value="items/staffs/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Item getStuffs(@PathVariable String id) {
        return p.getStuffs(Integer.parseInt(id));
    }

    @RequestMapping(value="items/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public String removeItems(@PathVariable("id") String id) {
        Item item = p.getItems(Integer.parseInt(id));
        boolean wasOK = p.removeItem(item);
        if (wasOK){
            return "Товар убран из продажи.";
        }
        return "Невозможно удалить товар.";
    }

    @RequestMapping(value="items/{id}/{item}", method = RequestMethod.PUT)
    @ResponseBody
    public boolean addItem(@PathVariable String id, @PathVariable String item)
    {
        String[] tmp = item.split(" ");
        Item elem;
        if (tmp[2].equals("Pet")) {
            elem = new Item(Integer.parseInt(tmp[0]), tmp[1], Item.Types.Pet, Integer.parseInt(tmp[3]), Integer.parseInt(tmp[4]));
        }
        else if (tmp[2].equals("Staff")) {
            elem = new Item(Integer.parseInt(tmp[0]), tmp[1], Item.Types.Staff, Integer.parseInt(tmp[3]), Integer.parseInt(tmp[4]));
        }
        else elem = null;
        return p.addItem(elem);
    }

    @RequestMapping(value="items", method = RequestMethod.GET)
    @ResponseBody
    public List<Item> getItems() {
        return p.getItems();
    }


}